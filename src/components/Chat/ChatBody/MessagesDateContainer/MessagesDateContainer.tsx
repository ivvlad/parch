import React from 'react';

import Message from '../Message/Message';
import styles from './messagesDateContainer.module.css';

import { IMessageDateContainer } from '../../../../interfaces';
import { formattedDate } from '../../../../helpers/chat';

const MessagesDateContainer: React.FC<IMessageDateContainer> = ({ date, messages }) => {
  const updatedDate = formattedDate(date);

  return (
    <div className={styles.messagesWrapper}>
      <h4 className={`${styles.messagesDivider} messages-divider`}>
        {updatedDate}
      </h4>
      <ul className={styles.messagesList}>
        {messages.map((message) => (
          <Message 
            key={message.id} 
            message={message} 
          />
        ))}
      </ul>
    </div>
  );
};

export default MessagesDateContainer;
