import { 
  SET_MESSAGES, 
  ADD_MESSAGE, 
  SET_PRELOADER, 
  EDIT_MESSAGE, 
  SET_EDIT_MESSAGE_ID, 
  SET_EDIT_MODE, 
  DELETE_MESSAGE 
} from './chat.types';
import { IMessage } from '../../interfaces';
import { 
  ISetMessages, 
  IEditMessage, 
  IAddMessage, 
  IDeleteMessage, 
  IPreloaderAction, 
  IEditMode, 
  IEditMessageId 
} from './chat.interfaces';

export const setMessages = (payload:IMessage[]):ISetMessages => ({
  type: SET_MESSAGES,
  payload,
});

export const setEditMessage = (payload:IMessage[]):IEditMessage => ({
  type: EDIT_MESSAGE,
  payload,
});

export const addMessage = (payload:IMessage):IAddMessage => ({
  type: ADD_MESSAGE,
  payload,
});

export const deleteMessage = (payload:string):IDeleteMessage => ({
  type: DELETE_MESSAGE,
  payload,
});

export const setPreloader = (payload:boolean):IPreloaderAction => ({
  type: SET_PRELOADER,
  payload,
});

export const setEditMode = (payload:boolean):IEditMode => ({
  type: SET_EDIT_MODE,
  payload,
});

export const setEditMessageId = (payload:string):IEditMessageId => ({
  type: SET_EDIT_MESSAGE_ID,
  payload,
});