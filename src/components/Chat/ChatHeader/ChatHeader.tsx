/* eslint-disable react/prop-types */
import React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';

import styles from './chatHeader.module.css';
import searchIcon from './assets/search.svg';
import settingsIcon from './assets/settings.svg';

import { IChatHeader } from '../../../interfaces';
import { IRootState } from '../../../store/reducer';

// eslint-disable-next-line 
const ChatHeader: React.FC<IChatHeader> = ({ user, userCount, messagesCount, lastMessageTime }) => {
  const { chatName } = useSelector((state:IRootState) => ({
    chatName: state.chat.chatName,
  }));

  const formattedDate = `${moment(lastMessageTime).format('DD.MM.YYYY HH:mm')}`;
  
  return (
    <header className={`${styles.chatHeader} header`}>
      <div className={styles.menuWrapper}>
        <div className={styles.burgerWrapper}>
          <button className={styles.menuBurger} type="button">
            <span className="visually-hidden">Меню</span>
          </button>
        </div>
      </div>
  
      <div className={styles.chatInfoWrapper}>
        <h3 className={`${styles.chatName} header-title`}>{chatName}</h3>
        <div className={styles.chatInfo}>
          <div>
            <p className="header-users-count">{userCount}</p>
            <p className={styles.chatSeparator}>|</p>
            <p className="header-messages-count">{messagesCount}</p>
          </div>
          <p className="header-last-message-date">{formattedDate}</p>
        </div>
      </div>
  
      <div className={styles.accountWrapper}>
        <button className={styles.searchIcon} type="button">
          <img src={searchIcon} alt="Поиск" />
        </button>
        <button className={styles.settingsButton} type="button">
          <img src={settingsIcon} alt="Настройки" />
        </button>
        <a href="/#">
          <span className="visually-hidden">Account</span>
          <img className={styles.userAvatar} src={user.avatar} alt={`Аватар пользователя ${user.name}`} />
        </a>
      </div>
    </header>
  );
};

export default ChatHeader;