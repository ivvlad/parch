import React from 'react';
import { useSelector } from 'react-redux';

import styles from './chatBody.module.css';
import MessagesDateContainer from './MessagesDateContainer/MessagesDateContainer';

import { filterMessagesByDate } from '../../../helpers/chat';
import Preloader from '../../Preloader/Preloader';
import { IRootState } from '../../../store/reducer';
import MessageEdit from '../../../modals/messageEdit/MessageEdit';

// eslint-disable-next-line
const ChatBody: React.FC = () => {
  const { messages, preloader } = useSelector((state:IRootState) => ({
    messages: state.chat.messages,
    preloader: state.chat.preloader,
  }));

  const filteredMessages = filterMessagesByDate(messages);

  if (preloader) {
    return <Preloader visibility={preloader} />;
  }

  return (
    <>
      <MessageEdit />
      <main className={`${styles.messages} message-list`}>
        {filteredMessages.map(((data) => (
          <MessagesDateContainer 
            key={data[1][0].id} 
            date={data[0]} 
            messages={data[1]}
          />
        )))}
      </main>
    </>
  );
};

export default ChatBody;
