import { AxiosError } from 'axios';
import moment from 'moment';
import { IMessage, IUser } from '../interfaces';

export const errorHandler = (error:AxiosError):void => {
  if (error.response) {
    console.warn(error.response.data);
  } else if (error.request) {
    console.warn(error.request);
  } else {
    console.warn('Axios error: ', error.message);
  }
};

export const getUserFromMessages = (messages: IMessage[], userId: string):IUser => {
  const userInfo = messages.find((message) => (message.userId === userId));
  const user:IUser = { 
    userId, 
    avatar: userInfo?.avatar || '', 
    name: userInfo?.user || '', 
  };

  return user;
};

export const getUsersCount = (messages: IMessage[]):number => {
  const uniqueUsers:string[] = [];

  messages.forEach(message => !uniqueUsers.includes(message.userId) && uniqueUsers.push(message.userId));

  return uniqueUsers.length;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const filterMessagesByDate = (messages: IMessage[]):any[] => {
  let lastDate = '';
  const filteredMessages:any[] = []; // eslint-disable-line
  
  messages.forEach((message) => {
    const date = moment(message.createdAt).format('l');
    const index = filteredMessages?.length - 1 || 0;

    if (lastDate === date) {
      return filteredMessages[index][1].push(message);
    }

    lastDate = date;
    return filteredMessages.push([date, [message]]);
  });
   
  return filteredMessages;
};

export const formattedDate = (date:string):string => {
  const today = moment().add(0,'days').format('l');
  const yesterday = moment().add(-1,'days').format('l');
  const otherDate = moment(date).format('dddd, D MMMM');

  if (date === today) {
    return 'Today';
  } 
  
  if (date === yesterday) {
    return 'Yesterday';
  }

  return otherDate;
};

export const formattedTime = (date:string):string => moment(date).format('HH:mm');

export const uniqueId = ():string => Date.now().toString(36) + Math.random().toString(36).substring(2);

export const getUpdatedMessagesText = (messages:IMessage[], messageId:string, text:string):IMessage[] => [ ...messages.map(message => {
  if (message.id === messageId) {
    return { ...message, text, editedAt: new Date().toISOString(), };
  }

  return message;
})] || [];